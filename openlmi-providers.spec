%global logfile %{_localstatedir}/log/openlmi-install.log
%global required_konkret_ver 0.9.0-2
%global required_libuser_ver 0.60

%define _libexecdir %{_prefix}/libexec

Name:           openlmi-providers
Version:        0.4.2
Release:        4%{?dist}
Summary:        Set of basic CIM providers

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://fedorahosted.org/openlmi/
Source0:        http://fedorahosted.org/released/openlmi-providers/%{name}-%{version}.tar.gz

# Upstream name has been changed from cura-providers to openlmi-providers
Provides:       cura-providers = %{version}-%{release}
Obsoletes:      cura-providers < 0.0.10-1

# == Provider versions ==

# Don't use %%{version} and %%{release} later on, it will be overwritten by openlmi metapackage
%global providers_version %{version}
%global providers_release %{release}
%global providers_version_release %{version}-%{release}
# Providers built from this package need to be strictly
# matched, so that they are always upgraded together.
%global hw_version %{providers_version_release}
%global sw_version %{providers_version_release}
%global pwmgmt_version %{providers_version_release}
%global acct_version %{providers_version_release}
%global svc_version %{providers_version_release}
%global pcp_version %{providers_version_release}
%global journald_version %{providers_version_release}
%global realmd_version %{providers_version_release}

# Storage and networking providers are built out of tree
# We will require a minimum and maximum version of them
# to ensure that they are tested together.
%global storage_min_version 0.7.1
%global storage_max_version 0.8

%global nw_min_version 0.2.2
%global nw_max_version 0.3

BuildRequires:  cmake
BuildRequires:  konkretcmpi-devel >= %{required_konkret_ver}
BuildRequires:  sblim-cmpi-devel
BuildRequires:  cim-schema
# For openlmi-fan
BuildRequires:  lm_sensors-devel
# For openlmi-account
BuildRequires:  libuser-devel >= %{required_libuser_ver}
BuildRequires:  python-devel
# for openlmi-*-doc packages
BuildRequires:  konkretcmpi-python >= %{required_konkret_ver}
BuildRequires:  python-sphinx
# For openlmi-hardware
BuildRequires:  pciutils-devel
# For openlmi-logicalfile
BuildRequires:  libudev-devel
BuildRequires:  libselinux-devel
# For openlmi-realmd
BuildRequires:  dbus-devel
# For openlmi-mof-register script
Requires:       python
# for openlmi-journald
BuildRequires:  libsystemd-journal-devel
# sblim-sfcb or tog-pegasus
# (required to be present during install/uninstall for registration)
Requires:       cim-server
Requires(pre):  cim-server
Requires(preun): cim-server
Requires(post): cim-server
Requires:       pywbem
Requires(pre):  pywbem
Requires(preun): pywbem
Requires(post):  pywbem
Requires:       cim-schema
# for lmi.base.mofparse:
Requires:       openlmi-python-base = %{providers_version_release}

# XXX
# Just because we have wired python's scripts
# Remove in future
BuildRequires:  python-setuptools

Patch0:         openlmi-providers-0.4.2-disable-stack-protector-strong-for-gcc.patch


%description
%{name} is set of (usually) small CMPI providers (agents) for basic
monitoring and management of host system using Common Information
Model (CIM).

%package devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{providers_version_release}
Requires:       konkretcmpi-python >= %{required_konkret_ver}
Provides:       cura-providers-devel = %{providers_version_release}
Obsoletes:      cura-providers-devel < 0.0.10-1

%description devel
%{summary}.

%package -n openlmi-fan
Summary:        CIM provider for controlling fans
Requires:       %{name}%{?_isa} = %{providers_version_release}
Provides:       cura-fan = %{providers_version_release}
Obsoletes:      cura-fan < 0.0.10-1

%description -n openlmi-fan
%{summary}.

%package -n openlmi-fan-doc
Summary:        CIM fan provider documentation
Group:          Documentation
BuildArch:      noarch

%description -n openlmi-fan-doc
This package contains the documents for OpenLMI fan provider.

%package -n openlmi-powermanagement
Summary:        Power management CIM provider
Requires:       %{name}%{?_isa} = %{providers_version_release}
Provides:       cura-powermanagement = %{providers_version_release}
Obsoletes:      cura-powermanagement < 0.0.10-1

%description -n openlmi-powermanagement
%{summary}.

%package -n openlmi-powermanagement-doc
Summary:        Power management CIM provider documentation
Group:          Documentation
BuildArch:      noarch

%description -n openlmi-powermanagement-doc
This package contains the documents for OpenLMI power management provider.

%package -n openlmi-service
Summary:        CIM provider for controlling system services
Requires:       %{name}%{?_isa} = %{providers_version_release}
Provides:       cura-service = %{providers_version_release}
Obsoletes:      cura-service < 0.0.10-1

%description -n openlmi-service
%{summary}.

%package -n openlmi-service-doc
Summary:        CIM service provider documentation
Group:          Documentation
BuildArch:      noarch

%description -n openlmi-service-doc
This package contains the documents for OpenLMI service provider.

%package -n openlmi-account
Summary:        CIM provider for managing accounts on system
Requires:       %{name}%{?_isa} = %{providers_version_release}
Requires:       openlmi-indicationmanager-libs%{?_isa} = %{providers_version_release}
Requires:       libuser >= %{required_libuser_ver}
Provides:       cura-account = %{providers_version_release}
Obsoletes:      cura-account < 0.0.10-1

%description -n openlmi-account
%{summary}.

%package -n openlmi-account-doc
Summary:        CIM account provider documentation
Group:          Documentation
BuildArch:      noarch

%description -n openlmi-account-doc
This package contains the documents for OpenLMI account provider.

%package -n openlmi-python-base
Summary:        Python namespace package for OpenLMI python projects
Requires:       python-setuptools
Requires:       cmpi-bindings-pywbem
BuildArch:      noarch
Obsoletes:      openlmi-python < 0.1.0-1
Provides:       openlmi-python = %{providers_version_release}

%description -n openlmi-python-base
The openlmi-python-base package contains python namespace package
for all OpenLMI related projects running on python.

%package -n openlmi-python-providers
Summary:        Python namespace package for pywbem providers
Requires:       %{name} = %{providers_version_release}
Requires:       openlmi-python-base = %{providers_version_release}
BuildArch:      noarch

%description -n openlmi-python-providers
The openlmi-python-providers package contains library with common
code for implementing CIM providers using cmpi-bindings-pywbem.

%package -n openlmi-python-test
Summary:        OpenLMI test utilities
Requires:       %{name} = %{providers_version_release}
Requires:       openlmi-python-base = %{providers_version_release}
#Requires:       openlmi-tools >= 0.9
BuildArch:      noarch

%description -n openlmi-python-test
The openlmi-python-test package contains test utilities and base
classes for provider test cases.


%package -n openlmi-indicationmanager-libs
Summary:        Libraries for CMPI indication manager
Requires:       %{name}%{?_isa} = %{providers_version_release}

%description -n openlmi-indicationmanager-libs
%{summary}.

%package -n openlmi-indicationmanager-libs-devel
Summary:        Development files for openlmi-indicationmanager-libs
Requires:       %{name}%{?_isa} = %{providers_version_release}
Requires:       openlmi-indicationmanager-libs%{?_isa} = %{providers_version_release}

%description -n openlmi-indicationmanager-libs-devel
%{summary}.

%package -n openlmi
Summary:        OpenLMI managed system software components
Version:        1.0.1
Requires:       %{name} = %{providers_version_release}
BuildArch:      noarch
Requires:       tog-pegasus
# List of "safe" providers
Requires:       openlmi-powermanagement = %{pwmgmt_version}
Requires:       openlmi-account = %{acct_version}
Requires:       openlmi-service = %{svc_version}

# Not mandatory, out-of-tree providers
#Requires:       openlmi-storage >= %{storage_min_version}
Conflicts:      openlmi-storage >= %{storage_max_version}
#Requires:       openlmi-networking >= %{nw_min_version}
Conflicts:      openlmi-networking >= %{nw_max_version}

# Optional Providers
# This ensures that only the appropriate version is installed but does
# not install it by default. If these packages are installed, this will
# guarantee that they are updated to the appropriate version on upgrade.
Conflicts:      openlmi-pcp > %{pcp_version}
Conflicts:      openlmi-pcp < %{pcp_version}

Conflicts:      openlmi-journald > %{journald_version}
Conflicts:      openlmi-journald < %{journald_version}

Conflicts:      openlmi-realmd > %{realmd_version}
Conflicts:      openlmi-realmd < %{realmd_version}

Conflicts:      openlmi-hardware > %{hw_version}
Conflicts:      openlmi-hardware < %{hw_version}

Conflicts:      openlmi-software > %{sw_version}
Conflicts:      openlmi-software < %{sw_version}

%description -n openlmi
OpenLMI provides a common infrastructure for the management of Linux systems.
This package installs a core set of OpenLMI providers and necessary
infrastructure packages enabling the system to be managed remotely.

%package -n python-sphinx-theme-openlmi
Summary:        OpenLMI theme for Sphinx documentation generator
Requires:       python-sphinx
BuildArch:      noarch

%description -n python-sphinx-theme-openlmi
python-sphinx-theme-openlmi contains Sphinx theme for OpenLMI provider
documentation.


%prep
%setup -q
%patch0 -p1


%build
#Dirty workaround (TODO: fix mof/CMakeLists.txt)
rm mof/60_LMI_Hardware.mof
rm mof/60_LMI_Journald.mof
rm mof/60_LMI_LogicalFile.mof
rm mof/60_LMI_Realmd.mof
rm mof/60_LMI_Software.mof
rm mof/60_LMI_Software_MethodParameters.mof
rm mof/70_LMI_SoftwareIndicationFilters.mof
rm mof/60_LMI_PCP.mof

%{cmake} -DCRYPT_ALGS='"SHA512","SHA256","DES","MD5"' -DWITH-HARDWARE=OFF -DWITH-JOURNALD=OFF -DWITH-REALMD=OFF -DWITH-LOGICALFILE=OFF -DWITH-SOFTWARE=OFF

make -k %{?_smp_mflags} all doc

cd ..
pushd src/python
%{__python} setup.py build
popd # src/python

%install
make install/fast DESTDIR=$RPM_BUILD_ROOT -C build

# The log file must be created
mkdir -p "$RPM_BUILD_ROOT/%{_localstatedir}/log"
touch "$RPM_BUILD_ROOT/%logfile"

# The registration database and directories
mkdir -p "$RPM_BUILD_ROOT/%{_sharedstatedir}/openlmi-registration/mof"
mkdir -p "$RPM_BUILD_ROOT/%{_sharedstatedir}/openlmi-registration/reg"
touch "$RPM_BUILD_ROOT/%{_sharedstatedir}/openlmi-registration/regdb.sqlite"

# XXX
# Remove pythonies
# Don't forget to remove this dirty hack in the future
rm -rf "$RPM_BUILD_ROOT"/usr/bin/*.py
rm -rf "$RPM_BUILD_ROOT"/usr/lib/python*

pushd src/python
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
cp -p lmi/__init__.* $RPM_BUILD_ROOT%{python_sitelib}/lmi
popd # src/python

# documentation
install -m 755 -d $RPM_BUILD_ROOT/%{_docdir}/%{name}
install -m 644 README COPYING $RPM_BUILD_ROOT/%{_docdir}/%{name}
for provider in account fan power; do
    install -m 755 -d $RPM_BUILD_ROOT/%{_docdir}/%{name}/${provider}/admin_guide
    cp -pr build/doc/admin/${provider}/html/* $RPM_BUILD_ROOT/%{_docdir}/%{name}/${provider}/admin_guide
done
install -m 755 -d $RPM_BUILD_ROOT/%{_docdir}/%{name}/service/admin_guide
cp -pr build/doc/admin/service-dbus/html/* $RPM_BUILD_ROOT/%{_docdir}/%{name}/service/admin_guide

# sphinx theme
install -m 755 -d $RPM_BUILD_ROOT/%{python_sitelib}/sphinx/themes/openlmitheme
cp -pr tools/openlmitheme/* $RPM_BUILD_ROOT/%{python_sitelib}/sphinx/themes/openlmitheme/


%files
%dir %{_docdir}/%{name}
%doc %{_docdir}/%{name}/README
%doc %{_docdir}/%{name}/COPYING
%dir %{_datadir}/%{name}
%dir %{_sysconfdir}/openlmi
%config(noreplace) %{_sysconfdir}/openlmi/openlmi.conf
%{_datadir}/%{name}/05_LMI_Qualifiers.mof
%{_datadir}/%{name}/30_LMI_Jobs.mof
%{_libdir}/libopenlmicommon.so.*
%attr(755, root, root) %{_bindir}/openlmi-mof-register
%ghost %logfile
%dir %{_sharedstatedir}/openlmi-registration
%dir %{_sharedstatedir}/openlmi-registration/mof
%dir %{_sharedstatedir}/openlmi-registration/reg
%ghost %{_sharedstatedir}/openlmi-registration/regdb.sqlite

%files devel
%doc README COPYING
%{_bindir}/openlmi-doc-class2rst
%{_bindir}/openlmi-doc-class2uml
%{_libdir}/libopenlmicommon.so
%{_libdir}/pkgconfig/openlmi.pc
%dir %{_includedir}/openlmi
%{_includedir}/openlmi/openlmi.h
%{_datadir}/cmake/Modules/OpenLMIMacros.cmake
%{_datadir}/cmake/Modules/FindOpenLMI.cmake
%{_datadir}/cmake/Modules/FindCMPI.cmake
%{_datadir}/cmake/Modules/FindKonkretCMPI.cmake
%{_datadir}/cmake/Modules/FindOpenLMIIndManager.cmake

%files -n openlmi-fan
%doc README COPYING
%{_libdir}/cmpi/libcmpiLMI_Fan.so
%{_datadir}/%{name}/60_LMI_Fan.mof
%{_datadir}/%{name}/60_LMI_Fan.reg
%{_datadir}/%{name}/90_LMI_Fan_Profile.mof
%attr(755, root, root) %{_libexecdir}/pegasus/cmpiLMI_Fan-cimprovagt

%files -n openlmi-fan-doc
%{_docdir}/%{name}/fan/

%files -n openlmi-powermanagement
%doc README COPYING
%{_libdir}/cmpi/libcmpiLMI_PowerManagement.so
%{_datadir}/%{name}/60_LMI_PowerManagement.mof
%{_datadir}/%{name}/60_LMI_PowerManagement.reg
%{_datadir}/%{name}/90_LMI_PowerManagement_Profile.mof
%attr(755, root, root) %{_libexecdir}/pegasus/cmpiLMI_PowerManagement-cimprovagt

%files -n openlmi-powermanagement-doc
%{_docdir}/%{name}/power/

%files -n openlmi-service
%doc README COPYING
%{_libdir}/cmpi/libcmpiLMI_Service.so
%{_datadir}/%{name}/60_LMI_Service.mof
%{_datadir}/%{name}/60_LMI_Service.reg
%{_datadir}/%{name}/90_LMI_Service_Profile.mof
%attr(755, root, root) %{_libexecdir}/pegasus/cmpiLMI_Service-cimprovagt

%files -n openlmi-service-doc
%{_docdir}/%{name}/service/

%files -n openlmi-account
%doc README COPYING
%{_libdir}/cmpi/libcmpiLMI_Account.so
%{_datadir}/%{name}/60_LMI_Account.mof
%{_datadir}/%{name}/60_LMI_Account.reg
%{_datadir}/%{name}/90_LMI_Account_Profile.mof
%attr(755, root, root) %{_libexecdir}/pegasus/cmpiLMI_Account-cimprovagt

%files -n openlmi-account-doc
%{_docdir}/%{name}/account/

%files -n openlmi-python-base
%doc README COPYING
%dir %{python_sitelib}/lmi
%{python_sitelib}/lmi/__init__.py
%{python_sitelib}/openlmi-*
%{python_sitelib}/lmi/base/

%files -n openlmi-python-providers
%doc README COPYING
%dir %{python_sitelib}/lmi/providers
%{python_sitelib}/lmi/providers/*.py

%files -n openlmi-python-test
%doc README COPYING
%dir %{python_sitelib}/lmi/test
%{python_sitelib}/lmi/test/*.py

%files -n openlmi-indicationmanager-libs
%doc COPYING src/indmanager/README
%{_libdir}/libopenlmiindmanager.so.*

%files -n openlmi-indicationmanager-libs-devel
%doc COPYING src/indmanager/README
%{_libdir}/libopenlmiindmanager.so
%{_libdir}/pkgconfig/openlmiindmanager.pc
%{_includedir}/openlmi/ind_manager.h

%files -n openlmi
%doc COPYING README

%files -n python-sphinx-theme-openlmi
%doc COPYING README
%{python_sitelib}/sphinx/themes/openlmitheme/

%pre
# If upgrading, deregister old version
if [ "$1" -gt 1 ]; then
    %{_bindir}/openlmi-mof-register --just-mofs unregister \
        %{_datadir}/%{name}/05_LMI_Qualifiers.mof \
        %{_datadir}/%{name}/30_LMI_Jobs.mof || :;
fi >> %logfile 2>&1

%post
/sbin/ldconfig
if [ "$1" -ge 1 ]; then
    %{_bindir}/openlmi-mof-register --just-mofs register \
        %{_datadir}/%{name}/05_LMI_Qualifiers.mof \
        %{_datadir}/%{name}/30_LMI_Jobs.mof || :;
fi >> %logfile 2>&1

%preun
# Deregister only if not upgrading
if [ "$1" -eq 0 ]; then
    %{_bindir}/openlmi-mof-register --just-mofs unregister \
        %{_datadir}/%{name}/05_LMI_Qualifiers.mof \
        %{_datadir}/%{name}/30_LMI_Jobs.mof || :;
fi >> %logfile 2>&1

%postun -p /sbin/ldconfig

%post -n openlmi-indicationmanager-libs -p /sbin/ldconfig
%postun -n openlmi-indicationmanager-libs -p /sbin/ldconfig

%pre -n openlmi-fan
# If upgrading, deregister old version
if [ "$1" -gt 1 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_LMI_Fan.mof \
        %{_datadir}/%{name}/60_LMI_Fan.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus unregister \
        %{_datadir}/%{name}/90_LMI_Fan_Profile.mof || :;
fi >> %logfile 2>&1

%pre -n openlmi-powermanagement
if [ "$1" -gt 1 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_LMI_PowerManagement.mof \
        %{_datadir}/%{name}/60_LMI_PowerManagement.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus unregister \
        %{_datadir}/%{name}/90_LMI_PowerManagement_Profile.mof || :;
fi >> %logfile 2>&1

%pre -n openlmi-service
if [ "$1" -gt 1 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_LMI_Service.mof \
        %{_datadir}/%{name}/60_LMI_Service.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus unregister \
        %{_datadir}/%{name}/90_LMI_Service_Profile.mof || :;
fi >> %logfile 2>&1

%pre -n openlmi-account
if [ "$1" -gt 1 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_LMI_Account.mof \
        %{_datadir}/%{name}/60_LMI_Account.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus unregister \
        %{_datadir}/%{name}/90_LMI_Account_Profile.mof || :;
fi >> %logfile 2>&1


%post -n openlmi-fan
# Register Schema and Provider
if [ "$1" -ge 1 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_LMI_Fan.mof \
        %{_datadir}/%{name}/60_LMI_Fan.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus register \
        %{_datadir}/%{name}/90_LMI_Fan_Profile.mof || :;
fi >> %logfile 2>&1

%post -n openlmi-powermanagement
if [ "$1" -ge 1 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_LMI_PowerManagement.mof \
        %{_datadir}/%{name}/60_LMI_PowerManagement.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus register \
        %{_datadir}/%{name}/90_LMI_PowerManagement_Profile.mof || :;
fi >> %logfile 2>&1

%post -n openlmi-service
if [ "$1" -ge 1 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_LMI_Service.mof \
        %{_datadir}/%{name}/60_LMI_Service.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus register \
        %{_datadir}/%{name}/90_LMI_Service_Profile.mof || :;
fi >> %logfile 2>&1

%post -n openlmi-account
if [ "$1" -ge 1 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} register \
        %{_datadir}/%{name}/60_LMI_Account.mof \
        %{_datadir}/%{name}/60_LMI_Account.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus register \
        %{_datadir}/%{name}/90_LMI_Account_Profile.mof || :;
fi >> %logfile 2>&1

%preun -n openlmi-fan
# Deregister only if not upgrading
if [ "$1" -eq 0 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_LMI_Fan.mof \
        %{_datadir}/%{name}/60_LMI_Fan.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus unregister \
        %{_datadir}/%{name}/90_LMI_Fan_Profile.mof || :;
fi >> %logfile 2>&1

%preun -n openlmi-powermanagement
if [ "$1" -eq 0 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_LMI_PowerManagement.mof \
        %{_datadir}/%{name}/60_LMI_PowerManagement.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus unregister \
        %{_datadir}/%{name}/90_LMI_PowerManagement_Profile.mof || :;
fi >> %logfile 2>&1

%preun -n openlmi-service
if [ "$1" -eq 0 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_LMI_Service.mof \
        %{_datadir}/%{name}/60_LMI_Service.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus unregister \
        %{_datadir}/%{name}/90_LMI_Service_Profile.mof || :;
fi >> %logfile 2>&1

%preun -n openlmi-account
if [ "$1" -eq 0 ]; then
    %{_bindir}/openlmi-mof-register -v %{providers_version} unregister \
        %{_datadir}/%{name}/60_LMI_Account.mof \
        %{_datadir}/%{name}/60_LMI_Account.reg || :;
    %{_bindir}/openlmi-mof-register --just-mofs -n root/interop -c tog-pegasus unregister \
        %{_datadir}/%{name}/90_LMI_Account_Profile.mof || :;
fi >> %logfile 2>&1

%changelog
* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu Jan 09 2014 Radek Novacek <rnovacek@redhat.com> 0.4.2-2
- Fix version mismatch caused by defining version of openlmi metapackage

* Wed Jan  8 2014 Jan Safranek <jsafrane@redhat.com> 0.4.2-1
- New upstream release

* Wed Nov  6 2013 Tomas Bzatek <tbzatek@redhat.com> 0.4.1-2
- Added explicit dependency on new libuser release

* Mon Nov  4 2013 Jan Safranek <jsafrane@redhat.com>> 0.4.1-1
- Version 0.4.1
- Add powermanagement and hardware providers documentation
- Require cim-schema
- Explicit dependency on systemd-libs removed
- Use python2_sitelib instead of python_sitelib
- Remove dependency on sblim-cmpi-base

* Wed Oct 16 2013 Radek Novacek <rnovacek@redhat.com> 0.4.0-2
- Fix wrong provider name in metapackage (power -> powermanagement)

* Tue Oct 15 2013 Tomas Bzatek <tbzatek@redhat.com> 0.4.0-1
- Version 0.4.0
- New journald provider
- Added documentation for service, logicalfile and realmd providers
- Documentation foundation improvements
- logicalfile and software misc. fixes
- Use PG_ComputerSystem by default
- Added documentation for software.

* Tue Oct 15 2013 Radek Novacek <rnovacek@redhat.com> 0.3.0-3
- Add openlmi metapackage

* Wed Oct  2 2013 Jan Safranek <jsafrane@redhat.com> - 0.3.0-2
- Added python-sphinx-theme-openlmi subpackage so we can build
  nightly builds on Rawhide.

* Tue Oct 01 2013 Roman Rakus <rrakus@redhat.com> - 0.3.0-1
- Version 0.2.0
- Many bugfixes.
- Enhancement in account api.
- per provider configuration files as well as global configuration files

* Fri Sep 20 2013 Roman Rakus <rrakus@redhat.com> - 0.2.0-3
- Define crypto algorithms for passwords in Account providers

* Wed Aug 28 2013 Michal Minar <miminar@redhat.com> 0.2.0-2
- Fixed build of account documentation.

* Tue Aug 27 2013 Michal Minar <miminar@redhat.com> 0.2.0-1
- Version 0.2.0
- Enhancement in software api.
- Added openlmi-account-doc package with admin guide.
- Fixed installation of python packages.

* Thu Aug 15 2013 Jan Safranek <jsafrane@redhat.com> 0.1.1-2
- Fixed registration into SFCB (#995561)
- Register __MethodParameters classes only in Pegasus

* Wed Aug 07 2013 Radek Novacek <rnovacek@redhat.com> 0.1.1-1
- Version 0.1.1
- Improve scripts logging
- Require dmidecode only on supported archs

* Wed Jul 31 2013  <jsafrane@redhat.com>  0.1.0-2
- Correctly obsolete openlmi-python package
- Remove dependency between openlmi-python-base and openlmi-providers

* Wed Jul 31 2013 Radek Novacek <rnovacek@redhat.com> 0.1.0-1
- Version 0.1.0
- Add profile registration
- New provider: openlmi-pcp
- Split openlmi-python to openlmi-python-base and openlmi-python-providers

* Mon Jul 15 2013 Jan Synáček <jsynacek@redhat.com> - 0.0.25-3
- Rebuild against new konkretcmpi
- Really fix the compilation against new konkretcmpi

* Fri Jun 28 2013 Roman Rakus <rrakus@redhat.com> - 0.0.25-2
- Againg add registration of 05_LMI_Qualifiers.mof

* Mon Jun 03 2013 Roman Rakus <rrakus@redhat.com> - 0.0.25-1
- Version 0.0.25

* Tue May 21 2013 Radek Novacek <rnovacek@redhat.com> 0.0.23-1
- Version 0.0.23
- Remove unused libexec files from services provider

* Fri May 17 2013 Jan Safranek <jsafrane@redhat.com> 0.0.22-3
- Remove duplicate definition of LMI_ConcreteJob

* Fri May 17 2013 Radek Novacek <rnovacek@redhat.com> 0.0.22-2
- Add registration of 05_LMI_Qualifiers.mof

* Fri May 10 2013 Jan Safranek <jsafrane@redhat.com> 0.0.22-1
- Version 0.0.22

* Thu May 09 2013 Radek Novacek <rnovacek@redhat.com> 0.0.21-3
- Fix wrong condition when registering provider

* Tue May 07 2013 Michal Minar <miminar@redhat.com> 0.0.21-2
- Software indication filters get (un)installed in pre/post scripts.

* Fri May 03 2013 Radek Novacek <rnovacek@redhat.com> 0.0.21-1
- Version 0.0.21
- Added openlmi-realmd subpackage
- Add numeral prefix for the mof files
- Add FindOpenLMI.cmake and openlmi.pc to the -devel subpackage

* Fri Mar 22 2013 Michal Minar <miminar@redhat.com> 0.0.20-1
- Version 0.0.20
- Added shared LMI_Jobs.mof file.
- This file is registered upon installation of openlmi-providers.

* Mon Mar 11 2013 Radek Novacek <rnovacek@redhat.com> 0.0.19-1
- Version 0.0.19
- Add LogicalFile provider
- Require konkretcmpi >= 0.9.0-2
- Install openlmi-doc-class2* scripts

* Thu Feb 14 2013 Radek Novacek <rnovacek@redhat.com> 0.0.18-2
- Add upstream patch with improvements of OpenLMIMacros.cmake
- Add requirement for cim-server also to pre, preun and post

* Mon Feb 04 2013 Michal Minar <miminar@redhat.com> 0.0.18-1
- Version 0.0.18

* Tue Jan 22 2013 Roman Rakus <rrakus@redhat.com> - 0.0.17-1
- Version 0.0.17

* Wed Jan 16 2013 Roman Rakus <rrakus@redhat.com> - 0.0.16-1
- Version 0.0.16

* Fri Dec 14 2012 Radek Novacek <rnovacek@redhat.com> 0.0.15-4
- Allow number in class name in OpenLMIMacros.cmake

* Fri Dec 07 2012 Radek Novacek <rnovacek@redhat.com> 0.0.15-3
- Add mof file with definition of 'Implemented' qualifier

* Fri Nov 23 2012 Radek Novacek <rnovacek@redhat.com> 0.0.15-2
- Add missing dependency on sblim-cmpi-base for powermanagement and account

* Tue Nov 13 2012 Radek Novacek <rnovacek@redhat.com> 0.0.15-1
- Version 0.0.15
- Upstream changed license from GPLv2+ to LGPLv2+

* Thu Nov 08 2012 Michal Minar <miminar@redhat.com> 0.0.14-1
- Version 0.0.14
- Added python subpackage for python providers

* Wed Oct 31 2012 Roman Rakus <rrakus@redhat.com> - 0.0.12-2
- Requires fixed konkretcmpi >= 0.8.7-7

* Thu Oct 25 2012 Roman Rakus <rrakus@redhat.com> - 0.0.12-1
- Version 0.0.12

* Thu Oct 25 2012 Radek Novacek <rnovacek@redhat.com> 0.0.10-2
- Remove %isa from obsoletes

* Mon Oct 22 2012 Radek Novacek <rnovacek@redhat.com> - 0.0.10-1
- Package rename to openlmi-providers (instead of cura-providers)
- helper scripts moved from usr/share to proper location
- Version 0.0.10

* Sat Oct 13 2012 Michal Minar <miminar@redhat.com> - 0.0.9-1
- Version 0.0.9
- Renamed cura-yum subpackage to cura-software

* Sun Oct 07 2012 Michal Minar <miminar@redhat.com> - 0.0.8-1
- Version 0.0.8

* Wed Oct 03 2012 Michal Minar <miminar@redhat.com> - 0.0.7-1
- Version 0.0.7

* Thu Sep 27 2012 Michal Minar <miminar@redhat.com> - 0.0.6-1
- Added yum software management providers.

* Thu Sep 27 2012 Roman Rakus <rrakus@redhat.com> - 0.0.5-1
- Version 0.0.5

* Tue Sep 18 2012 Roman Rakus <rrakus@redhat.com> - 0.0.4-1
- Version 0.0.4
- Remove python parts. There are not intended to be present here.

* Mon Aug 27 2012 Radek Novacek <rnovacek@redhat.com> 0.0.3-1
- Version 0.0.3
- Rename prefix from Cura_ to LMI_
- Add more development files

* Tue Aug 14 2012 Roman Rakus <rrakus@redhat.com> - 0.0.2-1
- Version 0.0.2 which includes account manager

* Fri Aug 03 2012 Radek Novacek <rnovacek@redhat.com> 0.0.1-2
- BR: cim-schema
- Don't clean buildroot in install
- Fix typo

* Tue Jul 31 2012 Radek Novacek <rnovacek@redhat.com> 0.0.1-1
- Initial package

